var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var User = (function () {
    //Constructor
    function User(name, email, age) {
        this.name = name;
        this.email = email;
        this.age = age;
        console.log("User created: " + name);
    }
    //Method
    User.prototype.register = function () {
        console.log("User " + this.name + " is now registered");
    };
    return User;
}());
//Instantiate a new User
var john = new User("John", "John@com.bg", 22);
console.log(john);
// Inheritance
var Member = (function (_super) {
    __extends(Member, _super);
    function Member(name, email, age, id) {
        _super.call(this, name, email, age);
        this.id = id;
    }
    return Member;
}(User));
//# sourceMappingURL=classes.js.map