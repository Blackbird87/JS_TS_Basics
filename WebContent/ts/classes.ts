class User
{
    
    //Primitive variables
    private name:string;
    private email:string;
    private age:number;

    //Constructor
    constructor(name:string , email:string , age:number)
    {
        this.name = name ;
        this.email = email ; 
        this.age = age ;
        
        console.log("User created: " + name);
    }
    
    //Method
    register()
    {
        console.log("User " + this.name + " is now registered");
    }  
}


//Instantiate a new User
let john = new User("John" , "John@com.bg" , 22 );

console.log(john);



// Inheritance
class Member extends User
{
    id:number ; 

    constructor(name:string , email:string , age:number , id:number)
    {
        super(name , email ,age);
        this.id = id ;
    }

}





