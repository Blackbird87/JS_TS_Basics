//--------------------------
//function with 2 parameters 

function getSum( num1: number, num2: number ): number {
    return num1 + num2;
};
console.log( getSum( 22, 44 ) )




//--------------------------
//function with 2 parameters 

let mySum = function( num1: number, num2: number ): number {
    return num1 + num2;
}

console.log( mySum( 3, 5 ) );




//--------------------------
// Optional parameter lastName
function getName( firstName: string, lastName?: string ): string {
    if ( lastName == undefined || lastName == null ) {
        return firstName;
    }

    return firstName + lastName;
}
console.log(getName("John","Doe")); // with 2 parameters
console.log(getName( "John")); // with 1 parameter

//--------------------------
//function void
function myVoid():void
{
   
}






