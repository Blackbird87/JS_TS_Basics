//--------------------------
//function with 2 parameters 
function getSum(num1, num2) {
    return num1 + num2;
}
;
console.log(getSum(22, 44));
//--------------------------
//function with 2 parameters 
var mySum = function (num1, num2) {
    return num1 + num2;
};
console.log(mySum(3, 5));
//--------------------------
// Optional parameter lastName
function getName(firstName, lastName) {
    if (lastName == undefined || lastName == null) {
        return firstName;
    }
    return firstName + lastName;
}
console.log(getName("John", "Doe")); // with 2 parameters
console.log(getName("John")); // with 1 parameter
//--------------------------
//function void
function myVoid() {
}
//# sourceMappingURL=functions.js.map